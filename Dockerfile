FROM alpine:3.7
RUN mkdir /app 
COPY ./app-exe /app/ 
WORKDIR /app 
ARG env
ARG config
ENV env=${env}
ENV config=${config}
CMD ["/app/app-exe"]
