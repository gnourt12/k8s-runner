package api

import (
	"net/http"

	"github.com/labstack/echo"
)

// ServiceName ...
const ServiceName = "truongnv-lession1"

//HealthCheck : check service is working
func HealthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, true)
}

// GetServiceName : get service name
func GetServiceName(c echo.Context) error {
	return c.JSON(http.StatusOK, ServiceName)
}
