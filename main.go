package main

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/gnourt12/k8s-runner/api"
)

func main() {
	e := echo.New()
	// handler mapping
	e.GET("/healthcheck", api.HealthCheck)
	e.GET("/healthz", api.HealthCheck)
	e.GET("/readyz", api.HealthCheck)
	e.GET("/service", api.GetServiceName)
	// expose
	err := e.Start(":80")
	if err != nil {
		fmt.Println(err.Error())
	}

}
